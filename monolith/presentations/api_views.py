from django.http import JsonResponse
from .models import Presentation, Status
from events.models import Conference
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
import pika


class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["name"]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title", "status"]

    encoders = {"status": StatusEncoder()}


class ConferenceEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class PresentationDetialEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]

    encoders = {
        "conference": ConferenceEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    # ]
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations}, encoder=PresentationListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            status = Status.objects.get(name=content["status"]["name"])
            content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse({"message": "Invalid status name"}, status=400)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        presentation = Presentation.objects.create(**content)
        return JsonResponse(
            presentation, encoder=PresentationDetialEncoder, safe=False
        )


@require_http_methods(["GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentations = Presentation.objects.get(id=id)
        # conference = {
        #     "name": presentations.conference.name,
        #     "href": presentations.conference.get_api_url(),
        # }
        # response = {
        #     "presenter_name": presentations.presenter_name,
        #     "company_name": presentations.company_name,
        #     "presenter_email": presentations.presenter_email,
        #     "title": presentations.title,
        #     "synopsis": presentations.synopsis,
        #     "created": presentations.created,
        #     "status": presentations.status.name,
        #     "conference": conference,
        # }
        return JsonResponse(
            presentations, encoder=PresentationDetialEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            status = Status.objects.get(name=content["status"]["name"])
            content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse({"message": "Invalid status name"}, status=400)
        try:
            conference = Conference.objects.get(id=id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            {"presenation": presentation}, encoder=PresentationDetialEncoder
        )


def send_message(title, body):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="tasks")
    channel.basic_publish(
        exchange="",
        routing_key=title,
        body=json.dumps(body),
    )
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation_name = presentation.presenter_name
    presentation_email = presentation.presenter_email
    presentation_title = presentation.title
    presentation.approve()
    message = {
        "presenter_name": presentation_name,
        "presenter_email": presentation_email,
        "title": presentation_title,
    }
    send_message("presentation_approvals", message)
    presentation.approve()
    return JsonResponse(
        presentation,
        encoder=PresentationDetialEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation_name = presentation.presenter_name
    presentation_email = presentation.presenter_email
    presentation_title = presentation.title
    presentation.approve()
    message = {
        "presenter_name": presentation_name,
        "presenter_email": presentation_email,
        "title": presentation_title,
    }
    send_message("presentation_rejections", message)
    presentation.approve()
    presentation.reject()
    return JsonResponse(
        presentation,
        encoder=PresentationDetialEncoder,
        safe=False,
    )


def approved_status(request, id):
    if request.method == "POST":
        status = Presentation.objects.get(id=id)
        status.approve()
        return JsonResponse({"approve": "APPROVED"})


def rejected_status(request, id):
    if request.method == "POST":
        status = Presentation.objects.get(id=id)
        status.reject()
        return JsonResponse({"approve": "REJECTED"})
