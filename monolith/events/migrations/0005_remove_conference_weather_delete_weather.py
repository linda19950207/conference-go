# Generated by Django 4.2.1 on 2023-05-11 21:30

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("events", "0004_weather_remove_location_description_and_more"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="conference",
            name="weather",
        ),
        migrations.DeleteModel(
            name="Weather",
        ),
    ]
