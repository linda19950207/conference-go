from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from events.acls import location_pictures, weather_report


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    # response = []
    if request.method == "GET":
        conferences = Conference.objects.all()
        # for conference in conferences:
        #     response.append(
        #         {
        #             "name": conference.name,
        #             "href": conference.get_api_url(),
        #         }
        #     )
        return JsonResponse(
            {"conferences": conferences}, encoder=ConferenceListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference, encoder=ConferenceDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT"])
def api_show_conference(request, id):
    conference = Conference.objects.get(id=id)
    if request.method == "GET":
        weather = weather_report(
            conference.location.city, conference.location.state.name
        )
        return JsonResponse(
            {"weather": weather, "conference": conference},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
        #     {
        #         "name": conference.name,
        #         "starts": conference.starts,
        #         "ends": conference.ends,
        #         "description": conference.description,
        #         "created": conference.created,
        #         "updated": conference.updated,
        #         "max_presentations": conference.max_presentations,
        #         "max_attendees": conference.max_attendees,
        #         "location": {
        #             "name": conference.location.name,
        #             "href": conference.location.get_api_url(),
        #         },
        #     }
        # )
    else:
        content = json.loads(request.body)
        if "location" in content:
            try:
                # location = Location.objects.get(id=content["location"])
                location = conference.location
                location.name = content["location"]["name"]
                location.save()
            except Location.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid location id"}, status=400
                )
            del content["location"]
        Conference.objects.filter(id=id).update(**content)
        # conference = Conference.objects.get(id=id)
        return JsonResponse(
            {"conference": conference}, encoder=ConferenceDetailEncoder
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        # response = [
        #     {"name": location.name, "href": location.get_api_url()}
        #     for location in locations
        # ]
        return JsonResponse(
            {"locations": locations}, encoder=LocationListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )
        picture_url = location_pictures(content["city"], content["state"])
        content.update(picture_url)
        location = Location.objects.create(**content)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
    if request.method == "GET":
        locations = Location.objects.get(id=id)
        # response = {
        #     "name": locations.name,
        #     "city": locations.city,
        #     "room_count": locations.room_count,
        #     "created": locations.created,
        #     "updated": locations.updated,
        #     "state": locations.state.abbreviation,
        # }
        return JsonResponse(
            locations, encoder=LocationDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            {"location": location}, encoder=LocationDetailEncoder
        )
