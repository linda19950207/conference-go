import json
import requests
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def location_pictures(city, state):
    url = "https://api.pexels.com/v1/search"
    query = {"query": f"{city}, {state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=query, headers=headers)
    clue = json.loads(response.content)
    try:
        picture_url = {"picture_url": clue["photos"][0]["src"]["original"]}
        return picture_url
    except (KeyError, IndexError):
        return {"picture_url": None}


def weather_report(city, state):
    # Get lat and long of city and state
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city}, {state}", "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]
    # Use lat and long to get weather
    url = "http://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    description = content["weather"][0]["description"]
    temp = content["main"]["temp"]
    return {
        "description": description,
        "temp": temp,
    }
