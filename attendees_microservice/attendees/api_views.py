from django.http import JsonResponse
from .models import Attendee, ConferenceVO, AccountVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeesListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id=None):
    conference_href = f"/api/conferences/{conference_id}/"
    conference = ConferenceVO.objects.get(import_href=conference_href)
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference.id)
        # response = [
        #     {"name": attendee.name, "href": attendee.get_api_url()}
        #     for attendee in attendees
        # ]
        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeesListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            # conference_href = f"/api/conferences/{conference_id}/"
            # conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailsEncoder, safe=False
        )


class ConferenceEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name"]


class AttendeeDetailsEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        print("///////", count)
        return {"has_account": count > 0}
        # if count > 0:
        #     return {"has_account": True}
        # else:
        #     return {"has_account": False}


@require_http_methods(["GET", "PUT"])
def api_show_attendee(request, id):
    show_attendee = Attendee.objects.get(id=id)
    if request.method == "GET":
        return JsonResponse(
            {"show_attendee": show_attendee},
            encoder=AttendeeDetailsEncoder,
            safe=False,
        )
        # conference = {
        #     "name": attendees.conference.name,
        #     "href": attendees.conference.get_api_url(),
        # }
        # response = {
        #     "email": attendees.email,
        #     "name": attendees.name,
        #     "company_name": attendees.company_name,
        #     "created": attendees.created,
        #     "conference": conference,
        # }
    else:
        content = json.loads(request.body)
        try:
            conference = ConferenceVO.objects.get(id=content["conference"])
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            {"attendee": attendee},
            encoder=AttendeeDetailsEncoder,
            safe=False,
        )


def add_badge(request, id):
    if request.method == "POST":
        attendee = Attendee.objects.get(id=id)
        attendee.create_badge()
        return JsonResponse({"badge": True})
