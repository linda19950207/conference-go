# Generated by Django 4.2.1 on 2023-05-18 16:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0004_remove_accountvo_updated'),
    ]

    operations = [
        migrations.AddField(
            model_name='accountvo',
            name='updated',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
