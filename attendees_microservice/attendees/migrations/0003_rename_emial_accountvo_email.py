# Generated by Django 4.2.1 on 2023-05-17 21:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0002_accountvo'),
    ]

    operations = [
        migrations.RenameField(
            model_name='accountvo',
            old_name='emial',
            new_name='email',
        ),
    ]
